#include <stdio.h>
#include <stdlib.h>

void funcion_recursiva(int (*vector), int longitud, int indice, int suma);
int main()
{
    int vector[40];
    for (int i = 0; i < 40; ++i){
        vector[i] = i;
    }
    funcion_recursiva(&vector[0], 40, 0, 0);
    system("pause");
    return 0;
}

void funcion_recursiva(int (*vector), int longitud, int indice, int suma){
    if(longitud > indice){
        *vector > 10 ? (suma += *vector) : NULL;
        ++vector, ++indice;
        return funcion_recursiva(&vector[0], longitud, indice, suma);
    } else {
        printf("La suma de los valores mayores a 10 de un vector de enteros de 40 posiciones es: %d \n", suma);
    }
}