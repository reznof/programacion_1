#include <stdio.h>
#include <stdlib.h>
void cargar_vector(int (*puntero), int longitud_fila); // se espera un puntero y 1 longitudes
void leer_vector(int (*puntero), int longitud_fila); // se espera un puntero y 1 longitudes

int main () {



	int vector[5];
	cargar_vector(vector, 5);
	leer_vector(vector, 5);



	system("pause");
	return 0;
}

void cargar_vector(int (*puntero), int longitud_fila){
	int contador = 1;
	for (int fila = 0; fila < longitud_fila; ++fila){
		puntero[fila] = contador; // cambiando valor
		contador++;
	}
}

void leer_vector(int (*puntero), int longitud_fila){
	for (int fila = 0; fila < longitud_fila; ++fila){
		printf("[%d]: %d \n", fila, puntero[fila]);
	}
}
