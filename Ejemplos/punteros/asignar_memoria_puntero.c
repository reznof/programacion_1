#include <stdio.h>
#include <stdlib.h>

int main()
{

	// Ejemplo: malloc (asigna bit en 1)
	// asignar memoria a un puntero tipo int
	
	int * puntero = NULL;
	// definimos el peso en bytes de un tipo int
	int bytes = sizeof(int); 
	// casteamos a puntero int, el puntero void devuelto por malloc
	puntero = (int *) malloc(bytes); 

	// controlamos si reservo memoria
	if(puntero == NULL) {	
		printf("%s\n", "No reservo la memoria del puntero");
	} else {
		printf("%s\n", "Se reservo la memoria del puntero");
	}


	//------------------------------------------------------------------



	// Ejemplo: realloc 
	// aumenta la memoria del puntero, (requiere un puntero temporal, para evitar fugas de memoria)

	int * temporal_puntero = realloc(puntero, bytes);

	if(temporal_puntero != NULL) {
		// re-asignamos memoria a puntero
		puntero = temporal_puntero;
		printf("%s\n", "Se aumento la memoria al puntero");
	}


	//------------------------------------------------------------------


	// Ejemplo: free
	// liberar memoria 
	free(puntero); // libera memoria 
	puntero = NULL; // y dejamos el puntero en null, (obligatorio, porque ya no se va a usar)


	//------------------------------------------------------------------



	// Ejemplo: calloc (asigna bit en 0)
	puntero = (int *) calloc(1, bytes); // 1 * bytes


	// controlamos si reservo memoria
	if(puntero == NULL) {	
		printf("%s\n", "No reservo la memoria del puntero");
	} else {
		printf("%s\n", "Se reservo la memoria del puntero");
	}

	system("pause");
	return 0;
}