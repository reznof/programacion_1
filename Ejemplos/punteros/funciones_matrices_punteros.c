#include <stdio.h>
#include <stdlib.h>

void cargar_matriz(int (*puntero)[5], int longitud_fila, int longitud_columna); // se espera un puntero y 2 longitudes
void leer_matriz(int (*puntero)[5], int longitud_fila, int longitud_columna); // se espera un puntero y 2 longitudes

int main () {
	int matriz[4][5];
	cargar_matriz(matriz, 4, 5);
	leer_matriz(matriz, 4, 5);
	system("pause");
	return 0;
}

void cargar_matriz(int (*puntero)[5], int longitud_fila, int longitud_columna){
	int contador = 1;
	for (int fila = 0; fila < longitud_fila; ++fila){
		for (int columna = 0; columna < longitud_columna; ++columna){
			puntero[fila][columna] = contador;
			contador++;
		}
	}
}
void leer_matriz(int (*puntero)[5], int longitud_fila, int longitud_columna){
	for (int fila = 0; fila < longitud_fila; ++fila){
		for (int columna = 0; columna < longitud_columna; ++columna){
			printf("[%d][%d]: %d \n", fila, columna, puntero[fila][columna]);
		}
	}
}