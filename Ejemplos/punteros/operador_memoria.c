#include <stdio.h>
#include <stdlib.h>

int main()
{


	int e = -400;
	int * puntero;
	// operador de memoria &
	puntero = &e; 

	// imprime direccion de memoria
	printf("%p\n", &e); // e, direccion de memoria
	printf("%p\n", puntero); // e, direccion de memoria (puntero apunta a e)
	printf("%p\n", &puntero); // puntero, direccion de memoria 	


	system("pause");
	return 0;
}