#include <stdio.h>
#include <stdlib.h>
void cargar_puntero(int *puntero, int valor); // se espera un puntero y 1 valor
void leer_puntero(int *puntero);
int main()
{
	int * puntero;
	int bytes = sizeof(int); // tamaño de un entero en bytes

	// !!! Importante: el puntero requiere posicion de memoria
	// casteamos a puntero int, el puntero void devuelto por malloc
	puntero = (int *) malloc(bytes); // malloc asigna memoria al puntero
	cargar_puntero(puntero, 5);
	leer_puntero(puntero);

	system("pause");
	return 0;
}

void cargar_puntero(int *puntero, int valor) {
	*puntero = valor;
}

void leer_puntero(int *puntero) {
	printf("%d\n", *puntero);
	printf("%p\n", &puntero);
	printf("%p\n", puntero);
}