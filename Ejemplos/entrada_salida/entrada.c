#include <stdio.h>
#include <stdlib.h>

int main()
{

	// Ejemplo int
	int entero;
	scanf("%d", &entero);
	printf("%d\n", entero);

	// Ejemplo float
	float flotante;
	scanf("%f", &flotante);
	printf("%.3g\n", flotante);





	// Ejemplo String
	char cadena[50];
	scanf("%s", &cadena);
	printf("%s\n", cadena);



	// Ejemplo char
	char caracter;
	scanf("%c", &caracter);
	printf("%c\n", caracter);


	system("pause");	
	return 0;
}