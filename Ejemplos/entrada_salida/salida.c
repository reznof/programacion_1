#include <stdio.h>
#include <stdlib.h>

int main()
{




	// Ejemplo Float 

	// para imprimir float usamos %.1 // significa 1 digito = 0.1
	float flotante_1 = 0.1;
	// para imprimir float usamos %.2 // significa 2 digitos = 0.12
	float flotante_2 = 0.12;
	// para imprimir float usamos %.3 // significa 3 digitos = 0.123
	float flotante_3 = 0.123;

	printf("%.0g\n", flotante_1);
	printf("%.1g\n", flotante_1);
	printf("%.2g\n", flotante_2);
	printf("%.3g\n", flotante_3);




	// Ejemplo Integer

	// para imprimir un entero usamos %d
	int entero = 5;

	printf("%d\n", entero);

	// Ejemplo String

	// para imprimir un String usamos %s
	char string[] = "cadena";

	printf("%s\n", string);





	// Ejemplo char

	// para imprimir un String usamos %c
	char caracter = 'H';

	printf("%c\n", caracter);





	// Ejemplo pointer

	// para imprimir posicion de memoria usamos %p

	int e = -400;
	int * puntero;
	puntero = &e;
	
	printf("%p\n", &e); // e, direccion de memoria
	printf("%p\n", puntero); // e, direccion de memoria (puntero apunta a e)
	printf("%p\n", &puntero); // puntero, direccion de memoria 	

	// Ejemplo pointer, imprimir valor del puntero
	printf("%d\n", *puntero);



	// Ejemplo sizeof

	// para imprimir el valor de sizeof (devuelve el tipo size_t), usamos  %zu
	// size_t es un entero sin signo

	printf("%zu\n", sizeof(int)); // peso en bytes de int
	printf("%zu\n", sizeof(char)); // peso en bytes de char
	printf("%zu\n", sizeof(float)); // peso en bytes de float

	system("pause");
	return 0;
}