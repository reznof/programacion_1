#include <stdio.h>
#include <stdlib.h>
int multiplicar_entero(int A, int B);
int main()
{
  int A, B;
  int cargaA = 0;
  int cargaB = 0;
  int datoTeclado;
  int resultado;
  do {
    if(cargaB == 0) {
      printf("Ingrese un dato por teclado (entero), de B distinto de 4 y 6\n");
      scanf("%d", &datoTeclado);
      if(datoTeclado != 4 && datoTeclado != 6){
        B = datoTeclado;
        cargaB = 1;
      }
    } else {
      if (cargaA == 0) {
        printf("Ingrese un dato por teclado (entero), de A, mientras que A < B\n");
        scanf("%d", &datoTeclado);
        if(datoTeclado < B) {
          A = datoTeclado;
          cargaA = 1;
        }
      }
    }
  } while(cargaA == 0);
  resultado = multiplicar_entero(A, B);
  printf("El resultado es: %d\n", resultado);
  system("pause");
  return 0;
}
int multiplicar_entero(int A, int B) {
  int resultado = (int) A * B;
  return resultado;
}