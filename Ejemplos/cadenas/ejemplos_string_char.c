#include <stdio.h>
#include <stdlib.h>

int main()
{

	// Ejemplo 1: definicion de string (se comporta como un vector char)
	char cadena_1[] = "Hola";
	// ejemplo con %s
	printf("%s\n",cadena_1);
	// ejemplo con %c
	printf("%c",cadena_1[0]);
	printf("%c",cadena_1[1]);
	printf("%c",cadena_1[2]);
	printf("%c\n",cadena_1[3]);


	// Ejemplo 2: definicion de vector char, (no se comporta siempre como un string)
	char cadena_2[] = {'F', 'o', 'r', 'd'};
	// se imprime como %c, nunca como %s da problemas
	printf("%c",cadena_2[0]);
	printf("%c",cadena_2[1]);
	printf("%c",cadena_2[2]);
	printf("%c\n",cadena_2[3]);


	system("pause");
	return 0;
}