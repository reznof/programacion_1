#include <stdio.h>
#include <stdlib.h>
int main()
{

	// diferencias entre double y float
	// float tiene .7 de presicion
	// double tiene .16 de presicion

	int A = 1;
	int B = 3;


	double result = (double) A / B;
	printf("%s %.3g\n", "result", result);

	system("pause");
	return 0;
}