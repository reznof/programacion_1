#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// B. Generar un programa que permita realizar la suma de los elementos en las posiciones 1, 5, 7 y 9 de un vector de 12 posiciones donde cada uno de los valores son enteros. Cargar los elementos del vector por teclado. Devolver el resultado imprimiéndolo en pantalla y guardándolo en un archivo. 

int main()
{
  int vector[12];
  int entero;
  int suma_elementos = 0;
  for (int fila = 0; fila < 12; ++fila){
    printf("ingrese un valor entero \n");
    scanf("%d", &entero);
    vector[fila] = entero;
    if( 1 == fila || 5 == fila || 7 == fila || 9 == fila ){
      suma_elementos += vector[fila];
    }      
  }
  printf("la suma de los elementos es = %d \n", suma_elementos);
  char nombre_archivo[35];
  FILE *archivo;
  printf("Ingrese el nombre del archivo\n");
  scanf("%s", &nombre_archivo);
  strcat(nombre_archivo, ".txt");
  archivo = fopen(nombre_archivo , "w"); 
  if (archivo == NULL) {
      printf("Error al intentar abrir archivo \n");
      exit(1);
  } else {
    fprintf(archivo, "%d\n", suma_elementos);
    printf("Se creo el archivo: %s \n", nombre_archivo);
    fclose(archivo);
  }
  system("pause");
  return 0;
}