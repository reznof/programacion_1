#include <stdio.h>
#include <stdlib.h>

// E. Realizar una función recursiva que permita calcular la suma de valores enteros que se ingresan por teclado hasta que se lea un valor 66. Debe realizarse en forma recursiva. Incluir la invocación desde el programa principal.

int recursivo(int suma);
int main()
{
    int suma;
    suma = recursivo(0);
    printf("la suma de los valores es %d\n", suma);
    system("pause");
    return 0;
}

int recursivo(int suma){
  int datoTeclado;
  printf("%s\n", "Ingrese un valor por teclado");
  scanf("%d", &datoTeclado);
  if(datoTeclado == 66){
    return suma;
  } else {
    suma += datoTeclado;
    return recursivo(suma);
  }
}