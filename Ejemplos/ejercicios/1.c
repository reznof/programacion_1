#include <stdio.h>
#include <stdlib.h>

// A. Generar un programa que permita cargar valores enteros (int) de teclado y guardarlos en un vector. La carga finaliza cuando se lee un valor 90(noventa) ó 91(noventa y uno). En el caso de que se lea un valor 6(seis) o un valor 7(siete), los mismos no deben ser guardados en el vector pero debe continuarse con la secuencia de lectura de teclado.

int main()
{
	int continuar = 1;
	int vector[200];
	int contador = 0;
	int datoTeclado;
	do {
		printf("Ingrese un dato por teclado\n");
		scanf("%d", &datoTeclado);
		if(
			(90 == datoTeclado || 91 == datoTeclado) &&
			contador < 200
		) {
			continuar = 0;
		} else {
			if (6 != datoTeclado && 7 != datoTeclado) {
				vector[contador] = datoTeclado;
				printf("guarda en vector %d\n", vector[contador]);
				contador++;
			}
		}
	} while(continuar);
	for (int fila = 0; fila < contador; ++fila){
		printf("vector [%d] = %d \n", fila, vector[fila]);
	}	
	system("pause");
	return 0;
}