#include <stdio.h>
#include <stdlib.h>
double dividir_entre(int A, int B);

// D. Realizar una función que permita calcular la división entre dos valores enteros A y B (Resultado=A/B). A debe ser menor que B, y además, B debe ser diferente de 0, 14 y 16 para que pueda realizarse el cálculo. Incluir la invocación de la función desde un programa. A y B se ingresan por teclado.

int main()
{

  int A, B;
  int cargaA = 0;
  int cargaB = 0;
  int datoTeclado;
  double resultado;
  do {
    if(cargaB == 0) {
      printf("Ingrese un dato por teclado (entero), de B distinto de 0, 14, 16\n");
      scanf("%d", &datoTeclado);
      if(datoTeclado != 0 && datoTeclado != 14 && datoTeclado != 16){
        B = datoTeclado;
        cargaB = 1;
      }
    } else {
      if (cargaA == 0) {
        printf("Ingrese un dato por teclado (entero), de A, mientras que A < B\n");
        scanf("%d", &datoTeclado);
        if(datoTeclado < B) {
          A = datoTeclado;
          cargaA = 1;
        }
      }
    }

  } while(cargaA == 0);

  resultado = dividir_entre(A, B);

  printf("El resultado es: %.3g\n", resultado);

  system("pause");
  return 0;
}

double dividir_entre(int A, int B) {
  double resultado = (double) A / B;
  return resultado;
}