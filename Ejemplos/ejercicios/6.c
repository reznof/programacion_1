#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// F. Generar un procedimiento recursivo que permita guardar en un archivo ocho valores enteros que se leen de teclado. Debe realizarse en forma recursiva. Incluir la invocación desde el programa principal.

void recursivo(int contador, FILE *(archivo));
int main()
{
    FILE *archivo;
    recursivo(0, archivo);
    system("pause");
    return 0;
}

void recursivo(int contador, FILE *(archivo)){
  int datoTeclado;
  if(contador == 0) {
    char nombre_archivo[35];
    printf("Ingrese el nombre del archivo\n");
    scanf("%s", &nombre_archivo);
    strcat(nombre_archivo, ".txt");
    archivo = fopen(nombre_archivo , "w");
    printf("%s\n", "Ingrese un valor por teclado");
    scanf("%d", &datoTeclado);
    fprintf(archivo,"%d\n", datoTeclado); 
    contador++;
    return recursivo(contador, archivo);
  } else if (contador != 8) {
    printf("%s\n", "Ingrese un valor por teclado");
    scanf("%d", &datoTeclado);
    fprintf(archivo,"%d\n", datoTeclado); 
    contador++;
    return recursivo(contador, archivo);
  } else {
    fclose(archivo);
  }
}