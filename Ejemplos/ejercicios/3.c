#include <stdio.h>
#include <stdlib.h>

// C. Generar un procedimiento que imprima en pantalla los valores de un vector de reales que tiene 370 posiciones. Debe realizarse en forma recursiva. Incluir la invocación desde el programa principal.


void recursivo_cargar_vector(int (*vector), int fila, int contador);
void recursivo_imprimir_vector(int (*vector), int fila, int contador);
int main()
{

    int vector[370];
    recursivo_cargar_vector(vector, 370, 0);
    recursivo_imprimir_vector(vector, 370, 0);
    system("pause");
    return 0;
}

void recursivo_cargar_vector(int (*vector), int fila, int contador){
  if(fila > contador){
    *vector = contador;
    ++vector;
    ++contador;
    return recursivo_cargar_vector(vector, fila, contador);
  }
}
void recursivo_imprimir_vector(int (*vector), int fila, int contador){
  if(fila > contador){
    printf("vector[%d]: %d \n", contador, *vector);
    ++vector;
    ++contador;
    return recursivo_imprimir_vector(vector, fila, contador);
  }
}