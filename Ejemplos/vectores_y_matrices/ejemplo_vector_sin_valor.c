#include <stdio.h>
#include <stdlib.h>

int main()
{
	// vector se define con fila explicita [4], o []
	int vector[4];

	int contador = 1;

	// recorro vector
	for (int fila = 0; fila < 4; ++fila){

		// cargo vector
		vector[fila] = contador;

		// imprimo valor
		printf("vector [%d] = %d \n", fila, vector[fila]);

		// aumento contador
		contador++;

	}

	system("pause");
	return 0;
}