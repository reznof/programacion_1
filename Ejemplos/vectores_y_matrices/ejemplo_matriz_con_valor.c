#include <stdio.h>
#include <stdlib.h>

int main()
{

	// matriz se define con fila explicita opcional [4], o []
	// pero con columna explicita obligatoria

	int matriz[4][5] = {{1,2,3,4,5}, {6,7,8,9,10}, {11,12,13,14,15}, {16,17,18,19,20}};

	// recorro matriz (fila [4], columna [5])
	for (int fila = 0; fila < 4; ++fila){
		for (int columna = 0; columna < 5; ++columna){

			// imprimo valor
			printf("matriz [%d][%d] = %d \n", fila, columna, matriz[fila][columna]);

		}
	}

	system("pause");
	return 0;
}