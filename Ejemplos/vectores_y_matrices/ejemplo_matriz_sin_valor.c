#include <stdio.h>
#include <stdlib.h>

int main()
{

	// matriz se define con fila explicita opcional [4], o []
	// pero con columna explicita obligatoria

	int matriz[4][5];

	int count = 1;

	// cargar matriz (fila [4], columna [5])
	for (int fila = 0; fila < 4; ++fila){
		for (int columna = 0; columna < 5; ++columna){

			// cargo matriz
			matriz[fila][columna] = count;

			// imprimo valor
			printf("matriz [%d][%d] = %d \n", fila, columna, matriz[fila][columna]);

			// aumento contador
			count++;
		}
	}
	
	system("pause");
	return 0;
}