#include <stdio.h>
#include <stdlib.h>

int main()
{
	// vector se define con fila explicita [4], o []
	int vector[4] = {1, 2, 3, 4};

	for (int fila = 0; fila < 4; ++fila){

		// imprimo valor
		printf("vector [%d] = %d \n", fila, vector[fila]);

	}

	system("pause");
	return 0;
}