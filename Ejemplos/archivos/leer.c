#include <stdlib.h>
#include <stdio.h>  

struct linea {
	char *contenido;
	int bytes;
	int contador;
	int cantidad_chars;
} linea;

int main(void)
{
	linea.contenido = NULL;
	linea.bytes = 0;
	linea.contador = 0;
	FILE *archivo = fopen("lectura.txt", "r+t");
	if (archivo == NULL) {
		printf("Error al intentar abrir archivo");
		exit(1);
	} else {
		linea.cantidad_chars = getline(&linea.contenido, &linea.bytes, archivo);
		while (linea.cantidad_chars >= 0) {
			printf("line[%d]: chars=%d, buf size=%d, contents: %s", linea.contador, linea.cantidad_chars, linea.bytes, linea.contenido);
			linea.cantidad_chars = getline(&linea.contenido, &linea.bytes, archivo);
			linea.contador++;
		}
		if(feof(archivo)) {
			printf("\n");
		}
		fclose(archivo);
	}
	system("pause");
	return 0;
}