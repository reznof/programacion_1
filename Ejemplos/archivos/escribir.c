#include <stdio.h>
#include <stdlib.h>
int main()
{
	// de modo de apertura
	// "w" 			: escritura, se crea si no existe o se sobreescribe si existe.
	// "a" 			: escritura al final, si no existe se crea.
	// "r+" 		: lectura y escritura, el fichero debe existir.
	// "w+" 		: lectura y escritura, se crea si no existe o se sobreescribe si existe.
	// "r+b ó rb+" 	: lectura y escritura, en modo binario para actualización.
	// "rb" 		: lectura, en modo binario

  char nombre_archivo[35];
  FILE *archivo;
  printf("Ingrese el nombre del archivo\n");
  scanf("%s", &nombre_archivo);
  strcat(nombre_archivo, ".txt");


  archivo = fopen(nombre_archivo , "w"); 

  if (archivo == NULL) {
      printf("Error al intentar abrir archivo");
      exit(1);
  } else {
    for (int contador = 0; contador < 5; ++contador) {
    	// inserta una linea formateada en el archivo
        fprintf(archivo,"%d\n", contador); 
    }
    printf("Se creo el archivo: %s \n", nombre_archivo);
  }
  fclose(archivo);
	
  system("pause");
	return 0;
}