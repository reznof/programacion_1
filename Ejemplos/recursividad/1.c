#include <stdio.h>
#include <stdlib.h>
void recursivo_imprimir_vector(int (*vector), int fila, int contador);
int main()
{
	int vector[4] = {1, 2, 3, 4};
    recursivo_imprimir_vector(vector, 4, 0);
    system("pause");
    return 0;
}

void recursivo_imprimir_vector(int (*vector), int fila, int contador){
	if(fila > contador){
		printf("vector[%d]: %d \n", contador, *vector);
		++vector;
		++contador;
		return recursivo_imprimir_vector(vector, fila, contador);
	}
}