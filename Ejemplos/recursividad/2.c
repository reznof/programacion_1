#include <stdio.h>
#include <stdlib.h>
void recursivo_cargar_vector(int (*vector), int fila, int contador);
void recursivo_imprimir_vector(int (*vector), int fila, int contador);
int main()
{

    // cargar recursivamente un vector de 370
    int vector[370];
    recursivo_cargar_vector(vector, 370, 0);
    recursivo_imprimir_vector(vector, 370, 0);
    system("pause");
    return 0;
}

void recursivo_cargar_vector(int (*vector), int fila, int contador){
  if(fila > contador){
    *vector = contador;
    ++vector;
    ++contador;
    return recursivo_cargar_vector(vector, fila, contador);
  }
}
void recursivo_imprimir_vector(int (*vector), int fila, int contador){
  if(fila > contador){
    printf("vector[%d]: %d \n", contador, *vector);
    ++vector;
    ++contador;
    return recursivo_imprimir_vector(vector, fila, contador);
  }
}