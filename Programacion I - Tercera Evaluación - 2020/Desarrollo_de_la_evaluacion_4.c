#include <stdio.h>
#include <stdlib.h>
int main()
{
	int dato = 9;
	int * puntero;
	puntero = &dato;
	printf("La direccion de memoria del puntero a un entero (int) es: %p \n", puntero);
	printf("El valor referenciado del puntero a un entero (int) es: %d \n", *puntero);
    system("pause");
    return 0;
}