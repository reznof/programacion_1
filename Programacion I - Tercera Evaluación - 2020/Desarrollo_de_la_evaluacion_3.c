#include <stdio.h>
#include <stdlib.h>
void mostrar_archivo();

int main()
{
    mostrar_archivo();
    system("pause");
    return 0;
}

void mostrar_archivo(){
    char ch, nombre_archivo[25];
    FILE *fp;

    printf("Ingrese el nombre del archivo\n");
    gets(nombre_archivo);

    fp = fopen(nombre_archivo, "r");

    if (fp == NULL)
    {
        perror("Error no se pudo abrir el archivo.\n");
        exit(EXIT_FAILURE);
    }
    printf("Imprimiendo la cantidad de numeros:\n", nombre_archivo);

    while((ch = fgetc(fp)) != EOF) {
        printf("%c", ch);
    }
    if(fgetc(fp) == EOF){
        printf("\n");
    }
    fclose(fp);
}