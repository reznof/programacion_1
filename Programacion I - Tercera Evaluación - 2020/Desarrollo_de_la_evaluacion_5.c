#include <stdio.h>
#include <stdlib.h>
#define calcular_longitud_vector(a) sizeof(a)/sizeof(*a)
void cargar_vector(int (*m), int l_1);
void recursivo_imprimir_vector(int (*m), int l_1, int i);
int main()
{
    int vector[40];
    int longitud_v = calcular_longitud_vector(vector);
    cargar_vector(&vector[0], longitud_v);   
    recursivo_imprimir_vector(&vector[0], longitud_v, 0);
    system("pause");
    return 0;
}

void cargar_vector(int (*m), int l_1){
	char modo; // valores: [Y, N]::char
	printf("Desea cargar la matriz automaticamete? \n");
	printf("Presione 'Y' para cargar la matriz, o presione 'N' para que la carga sea manual \n\n");
	scanf("%c", &modo);
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		if(modo == 'Y' || modo == 'y'){
			*m = i_1 + 5;
		} else if(modo == 'N' || modo == 'n'){
			printf("Ingrese un valor int para cargar el vector[%d]: ", i_1);
			scanf("%d", m);
		}
		++m;
	}
}

void recursivo_imprimir_vector(int (*m), int l_1, int i){
	if(l_1 > i){
		printf("El valor de la matriz en posiciones [%d], es: %d \n", i, *m);
		++m;
		++i;
		return recursivo_imprimir_vector(&m[0], l_1, i);
	}
}