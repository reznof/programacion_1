#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define calcular_longitud_vector(a) sizeof(a)/sizeof(*a)
void cargar_vector(int (*m), int l_1);
void cargando_archivo(int (*m), int l_1);

int main()
{
    int vector[45];
    int longitud_v = calcular_longitud_vector(vector);
    cargar_vector(&vector[0], longitud_v);
    cargando_archivo(&vector[0], longitud_v);
    system("pause");
    return 0;
}

void cargar_vector(int (*m), int l_1){
    char modo; // valores: [Y, N]::char
    printf("Desea cargar la matriz automaticamete? \n");
    printf("Presione 'Y' para cargar la matriz, o presione 'N' para que la carga sea manual \n\n");
    scanf("%c", &modo);
    for (int i_1 = 0; i_1 < l_1; ++i_1){
        if(modo == 'Y' || modo == 'y'){
            *m = i_1 + 5;
        } else if(modo == 'N' || modo == 'n'){
            printf("Ingrese un valor int para cargar el vector[%d]: ", i_1);
            scanf("%d", m);
        }
        ++m;
    }
}

void cargando_archivo(int (*m), int l_1){
    char ch, nombre_archivo[25];
    FILE *fp;
    printf("Ingrese el nombre del archivo\n");
    scanf("%s", &nombre_archivo);
    strcat(nombre_archivo, ".txt");
    fp = fopen(nombre_archivo , "w"); 
    if (fp == NULL)
    {
        perror("Error no se pudo crear el archivo.\n");
        exit(EXIT_FAILURE);
    }
    for (int i_1 = 0; i_1 < l_1; ++i_1){
        fprintf(fp,"%d\n", *m); 
        ++m;
    }
    printf("Se creo el archivo: %s \n", nombre_archivo);
    fclose(fp);
}