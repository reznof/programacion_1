#include <stdio.h>
#include <stdlib.h>
#define calcular_longitud_vector(a) sizeof(a)/sizeof(*a) //define la cantidad en bytes sobre la cantidad de valores que apunta al puntero
void cargar_vector(int (*m), int l_1); // se espera un puntero y 1 longitudes
void leer_suma_vector(int *suma_vector); // se espera un puntero y 1 longitudes
void calcular_suma_vector(int (*m), int l_1, int *suma_vector); // se espera un puntero y 1 longitudes
void leyenda();
int main(){
	int vector[3];
	int suma_vector = 0;
	int longitud_v = calcular_longitud_vector(vector);
	leyenda();
	cargar_vector(&vector[0], longitud_v);
	calcular_suma_vector(&vector[0], longitud_v, &suma_vector);
	leer_suma_vector(&suma_vector);
	system("pause");
	return 0;
}
void leyenda(){
	printf("Programando en C \n\n");
	printf("Generar un programa que permita realizar la suma de los contenidos de las posiciones de un vector de enteros de 33 elementos, solamente si el valor de ese componente es mayor a 10. Utilizar lenguaje C.\n\n");
}
void cargar_vector(int (*m), int l_1){
	char modo; // valores: [Y, N]::char
	printf("Desea cargar la matriz automaticamete? \n");
	printf("Presione 'Y' para cargar la matriz, o presione 'N' para que la carga sea manual \n\n");
	scanf("%c", &modo);
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		if(modo == 'Y' || modo == 'y'){
			*m = i_1 + 10;
		} else if(modo == 'N' || modo == 'n'){
			printf("Ingrese un valor int para cargar el vector[%d]: ", i_1);
			scanf("%d", m);
		}
		++m;
	}
}
void calcular_suma_vector(int (*m), int l_1, int *suma_vector){
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		printf("calcular_suma_vector: El valor de la matriz en posiciones [%d], es: %d \n", i_1, *m);
		if(*m > 10){
			*suma_vector += *m; 
		}
		++m;
	}	
}

void leer_suma_vector(int *suma_vector){
	printf("El total de la suma es: %d \n", *suma_vector);
}