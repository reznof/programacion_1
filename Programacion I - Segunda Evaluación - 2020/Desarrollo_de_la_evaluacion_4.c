#include <stdio.h>
#include <stdlib.h>
#define calcular_longitud_vector(a) sizeof(a)/sizeof(*a) //define la cantidad en bytes sobre la cantidad de valores que apunta al puntero
void cargar_vector(int (*m), int l_1); // se espera un puntero y 1 longitudes
void leer_vector(int (*m), int l_1); // se espera un puntero y 1 longitudes
void leyenda();
int main(){
	int vector[15];
	int longitud_v = calcular_longitud_vector(vector);
	leyenda();
	cargar_vector(&vector[0], longitud_v);
	leer_vector(&vector[0], longitud_v);
	system("pause");
	return 0;
}
void leyenda(){
	printf("Programando en C \n\n");
	printf("Defina un tipo para un vector de 15 posiciones donde cada uno de los elementos son enteros. Es decir, cada posicion del vector debe contener un entero. Utilizar lenguaje C para definir la estructura solicitada\n\n");
}
void cargar_vector(int (*m), int l_1){
	char modo; // valores: [Y, N]::char
	printf("Desea cargar la matriz automaticamete? \n");
	printf("Presione 'Y' para cargar la matriz, o presione 'N' para que la carga sea manual \n\n");
	scanf("%c", &modo);
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		if(modo == 'Y' || modo == 'y'){
			*m = i_1 + 5;
		} else if(modo == 'N' || modo == 'n'){
			printf("Ingrese un valor int para cargar el vector[%d]: ", i_1);
			scanf("%d", m);
		}
		++m;
	}
}
void leer_vector(int (*m), int l_1){
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		printf("leer_matriz_vector: El valor de la matriz en posiciones [%d], es: %d \n", i_1, *m);
		++m;
	}
}