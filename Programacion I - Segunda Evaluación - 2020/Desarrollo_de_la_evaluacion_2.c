#include <stdio.h>
#include <stdlib.h>
void cargar_valores(int (*k), int (*n), int (*y));
float calcular_valores(int k, int n, int y);
void leyenda();
int main(){
	int k, n, y;
	leyenda();
	cargar_valores(&k, &n, &y);
	float calculo = calcular_valores(k, n, y);
	printf("imprimiendo en pantalla desde el programa principal (main): %f \n", calculo);
	system("pause");
	return 0;
}
void leyenda(){
	printf("Programando en C \n\n");
	printf("Se desea hacer el siguiente calculo: (k-n)*y/3, donde k, n e y son enteros (int) que se ingresan por teclado.\nSe debe utilizar una funcion para realizar el calculo. El resultado (float) debe imprimirse en pantalla desde el programa principal (main).\nConsiderar que k debe ser mayor que n para poder hacer el caculo. Utilizar lenguaje C.\n\n");
}
float calcular_valores(int k, int n, int y){
	 float calculo;
	 calculo = (k - n) * y / 3;
	 return(calculo);
}
void cargar_valores(int (*k), int (*n), int (*y)){
	do {
		printf("Ingrese el valor de k (int) \n");
		scanf("%d", k);
		printf("Ingrese el valor de n (int) \n");
		scanf("%d", n);
		if(k < n){
			printf("k debe ser mayor que n para poder hacer el caculo. \n\n");
		} else {
			printf("Ingrese el valor de y (int) \n");
			scanf("%d", y);
		}
	} while (k < n);
	printf("Los valores de k, n, f son: (k %d), (n %d), (f %d) \n", *k, *n, *y);
}