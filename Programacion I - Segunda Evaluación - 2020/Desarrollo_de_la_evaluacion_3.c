#include <stdio.h>
#include <stdlib.h>
void leyenda();
int main(){
	// ejecutando el subprograma leyenda, dentro del comportamiento del programa principal (main)
	leyenda();
	system("pause");
	return 0;
}
// subprograma leyenda: es un procedimiento que imprimir 2 leyendas
void leyenda(){
	printf("Programando en C \n\n");
	printf("Que es un subprograma?, Para que sirve? Desarrolle.\n\n");
	// ejecutando el subprograma leyenda, dentro del comportamiento del subprograma leyenda
	respuesta();
}
// subprograma leyenda: es un procedimiento que imprimir 1 respuesta
void respuesta(){
	printf("Los subprogramas son funciones o procedimientos y solo se ejecutan cuando son invocados desde el programa principal (main) o desde otros subprogramas.");
}