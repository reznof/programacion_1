#include <stdio.h>
#include <stdlib.h>
void leyenda();
void respuesta();
void ejemplo_while();
void ejemplo_for();
void leer_iteracion(int valor);
int main(){
	int i = 0;
	while(i < 5){
		leer_iteracion(i);
		i++;
	}
	return 0;
}
void leer_iteracion(int valor){
	printf("Iteracion: %d \n", valor);
}

void leyenda(){
	printf("Programando en C \n\n");
	printf("Cuales son las similitudes y diferencias entre el while y el for?, Para qué usaría cada uno (de al menos un ejemplo para cada instruccion)?.\n\n");
}

void respuesta(){
	printf("Las similitudes entre el while y el for es que ambas son estructuras iterativas. \n\n");
	printf("la diferencia es que el while se va a ejecutar una y otra vez hasta que x condición se torne falsa. En cambio el for se segmenta en 3 partes: en la primera parte se asigna o inicializa una variables, en la segunda parte se da una condicion se interpreta igual que el while, finalmente en la tercera parte se declara una instrucciones que se ejecutarán despues de una iteración (por genera se incrementa o disminuye el valor de la variable del primer segmento con los operadores ++, --) \n\n");
	printf("Usaria el for para iterar un vector o matriz, y utilizaria el while cuando debo evaluar una condicion para dejar de ejecutar un determinado comportamiento\n\n");
}
void ejemplo_while(){
	int i = 0;
	printf("Ejemplo de una ejecucion con el while");
	while(i < 5){
		leer_iteracion(i)
		i++;
	}
}

void leer_iteracion(int valor){
	printf("Iteracion: %d \n", valor);
}

void ejemplo_for(){
	printf("Ejemplo de una ejecucion con el for");
	for (int i = 0; i < 5; ++i){
		printf("Iterando con el bucle for el elemento i (solo si i es menor que 5): %d \n", i);
	}
}