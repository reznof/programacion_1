#include <stdio.h>
#include <stdlib.h>
#define calcular_longitud_vector(a) sizeof(a)/sizeof(*a)
void cargar_matriz_2D(float (*m)[5], int l_1, int l_2); 
void leer_matriz_2D(float (*m)[5], int l_1, int l_2); 
void leyenda(); 
int main(){
	float matriz[4][5];
	int longitud_1 = calcular_longitud_vector(matriz);
	int longitud_2 = calcular_longitud_vector(matriz[0]);
	leyenda();
	cargar_matriz_2D(matriz, longitud_1, longitud_2);
	leer_matriz_2D(matriz, longitud_1, longitud_2);
	longitud_1 = 0, longitud_2 = 0;
	system("pause");
	return 0;
}
void leyenda(){
	printf("Programando en C \n\n");
	printf("Ejercicio: Para que son utiles las estructuras del tipo matriz?\nDefina un tipo matriz de reales (float) de 4x5 posiciones y una variable de ese tipo como lo haria en un programa en C.\n\n");
}
void cargar_matriz_2D(float (*m)[5], int l_1, int l_2){
	char modo; // valores: [Y, N]::char
	printf("Desea cargar la matriz automaticamete? \n");
	printf("Presione 'Y' para cargar la matriz, o presione 'N' para que la carga sea manual \n\n");
	scanf("%c", &modo);
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		for (int i_2 = 0; i_2 < l_2; ++i_2){
			if(modo == 'Y' || modo == 'y'){
				m[i_1][i_2] = (i_1 + 1 + i_2 + 1) / 1.3;
			} else if(modo == 'N' || modo == 'n'){
				printf("Ingrese un valor de reales (float), para cargar la matriz en posiciones [%d][%d]: ", i_1, i_2);
				scanf("%f", &m[i_1][i_2]);
			}
		}
	}
}
void leer_matriz_2D(float (*m)[5], int l_1, int l_2){
	for (int i_1 = 0; i_1 < l_1; ++i_1){
		for (int i_2 = 0; i_2 < l_2; ++i_2){
			printf("El valor de la matriz en posiciones [%d][%d], es: %.3g \n", i_1, i_2, m[i_1][i_2]);
		}
	}
}